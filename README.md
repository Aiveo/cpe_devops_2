# DevOps 2 Report

Students: 

MORANDET Louis

LAURENT Jérémy

##  TP1 - Monitoring
### Monitoring your application

#### Checkpoint: Plug Prometheus/Grafana and your application all together.
**Notes:** We have choosen to not use frontend to recover backend metric. Because if the frontend is down, we can not get backend metric. 
Follow this step to plug Prometheus/Grafana to the application

 - Copy the project form DevOps 1
 - Add Grafana and prom service from TD1
 - Copy datasources.yml and dashboards.yml
 - Copy prometheus.yml and add the following job to supervise the backend

``` yml
  - job_name: backend
    metrics_path: /api/actuator/prometheus 
    scheme: http
    static_configs:
      - targets: ["backend:8080"]
```

Prometheus:

	http://localhost:9090/targets

Backend metrics:

	http://localhost/api/actuator/prometheus

	http://localhost/api/actuator/metrics

Grafana:

 	http://localhost:3000/dashboards

#### 1. What's the difference between the system CPU usage and the process CPU usage ?

Based on this [code source](https://github.com/micrometer-metrics/micrometer/blob/98bd8d3f2c31ced79c1fb6a21721172abc38bc21/micrometer-core/src/main/java/io/micrometer/core/instrument/binder/system/ProcessorMetrics.java#L102)
  
- **system.cpu.usage** -- The recent cpu usage for the whole system

- **process.cpu.usage** -- The recent cpu usage for the Java Virtual Machine process

For more info about these metrics you can look at java doc of this bean [OperatingSystemMXBean](https://docs.oracle.com/javase/8/docs/jre/api/management/extension/com/sun/management/OperatingSystemMXBean.html)

#### 2. What represents the CPU load ?

The CPU usage consists of the total CPU time in milliseconds for system threads, application threads, and resource-monitor threads

#### 3. What are all those types of thread ? 4. What do they all mean ? 

- **Live threads**: This shows the current number of live/active threads including both daemon and non-daemon threads (Currently running).

- **User Thread** --> User threads are threads which are created by the application or user. They are high priority threads. JVM (Java Virtual Machine) will not exit until all user threads finish their execution. JVM wait for these threads to finish their task. These threads are foreground threads. (I also include this answer 😊) 

-  **Daemon Thread** --> Daemon threads are threads which are mostly created by the JVM. These threads always run in background. These threads are used to perform some background tasks like garbage collection and house-keeping tasks. These threads are less priority threads. JVM will not wait for these threads to finish their execution. JVM will exit as soon as all user threads finish their execution. JVM doesn’t wait for daemon threads to finish their task.

**Total Threads**: This gives the total number of threads created and also started since the Java virtual machine started.

![Thread type](./Screenshots/CleanShot%202021-11-29%20at%2021.40.21@2x.png)

#### 5. Memory max / used / committed ?

- **used** --> The amount of used memory in bytes
-  **committed** --> Returns the amount of memory in bytes that is committed for the Java virtual machine to use. This amount of memory is guaranteed for the Java virtual machine to use.
-  **max** --> Returns the maximum amount of memory in bytes that can be used for memory management. This method returns -1 if the maximum memory size is undefined. 

![Memory](./Screenshots/CleanShot%202021-11-29%20at%2021.40.34@2x.png)

#### Checkpoint: "Show me your curves", in Grafana, of course.

### Monitoring your Database

In `docke-compose.yml` file :

``` yml
  database_exporter:
    container_name: database_exporter
    image: bitnami/postgres-exporter
    networks:
      - app-network
    environment:
      - DATA_SOURCE_URI=database?sslmode=disable
      - DATA_SOURCE_USER=takima
      - DATA_SOURCE_PASS=takimapass
```

In `prometheus.yml` file :

``` yml
  - job_name: database
    metrics_path: /metrics 
    scheme: http
    static_configs:
      - targets: ["database_exporter:9187"]
```
And create a new dashboard (or copy dashboard n°`9628` from grafana.com)

### Second Application

- Get the repository,
- Copy graphana, prometheus and postgres_exporter config (from `docker-compose.yml` and other config file)
- Change database and backend parameters (hostname,user and password in `docker-compose.yml` and `prometheus.yml`)
- Normally, it will work

## TP2 - ELK
### Monitoring Elasticsearch

#### 1. Which exporter did you use ? Describe your configuration.

We use `bitnami/elasticsearch-exporter` which is an implementation of the opensource project on GitHub

In `docker-compose.yml`: 
```yml
  elastic_exporter:
    image: bitnami/elasticsearch-exporter
    networks:
      - resa_net
    environment:
     - ES_URI=http://elasticsearch:9200
```

In `prometheus.yml`: 
```yml
- job_name: elsaticsearch
    metrics_path: /metrics 
    scheme: http
    static_configs:
      - targets: ["elastic_exporter:9114"]
```

### Collect application’s logs

#### 2. List the existing Beats in the Elastic world, and explain in 1-2 lines what data it is supposed to collect.
- **Filebeat** 
Filebeat is designed to read files from your system. It is particularly useful for system and application log files, but can be used for any text files that you would like to index to Elasticsearch in some way.

- **Metricbeat**  
Metricbeat is used to collect metrics from servers and systems. It is a lightweight platform dedicated to sending system and service statistics. 

- **Packbeat**  
Packetbeat, a lightweight network packet analyzer, monitors network protocols to enable users to keep tabs on network latency, errors, response times, SLA performance, user access patterns and more.

- **Winlogbeat**  
Winlogbeat is a tool specifically designed for providing live streams of Windows event logs. The raw data collected by Winlogbeat is automatically sent to Elasticsearch and then indexed for convenient future reference.

- **Auditbeat**  
Auditd event data is analyzed and sent, in real time, to Elasticsearch for monitoring the security of your environment.

- **Heartbeat**  
Heartbeat is a lightweight shipper for uptime monitoring. It monitors services basically by pinging them and then ships data to Elasticsearch for analysis and visualization

#### 3. List the steps you followed and the command you run in order to set up Filebeat.

In `docker-compose.yml`: 
```yml
  filebeats:
    image: docker.elastic.co/beats/filebeat:7.15.2
    networks:
      - resa_net
    volumes:
      - ./filebeat.yml:/usr/share/filebeat/filebeat.yml
      - /var/lib/docker/containers:/var/lib/docker/containers:ro
    user: root
```

In `filebeat.yml`:
```yml
output.elasticsearch:
  hosts: ["elasticsearch:9200"]

filebeat.inputs:
- type: docker
  containers.ids: "e1ec2715343e54a9d9978e4ba1bbee62f1b9f670124d07f4f5379aee403b101e"
```

#### 4. Checkpoint : validate your setup with us and document your work

#### 5. Explain how you managed to detect the problem, and how you fixed it or tried to fix it !

## TP3 - Loads Tests

Unfortunately, because of we have macbook pro with apple silicone (M1 ARM), some docker image give it to us some trouble. We have successed to figure out the problem (but at 17h30) with our amazing teachers.

### Questions
####  1. What are those parameters used for ?

 - **GATLING_RAMP_USERS** :  number of users for the test
 - **GATLING_RAMP_DURATION** : Duration of the test

Other parameters define the kind of http request is sent by Gatling while its tests.

#### 2. Why are the results improving with the number of times I run the tests ?
Thanks to using cach system, JVM become more efficient. That's the reason, why response time get lower through time.

#### 3. What can companies do to both absorb those previsible spikes while limiting their costs ?
They can:
- Rent server
- Use queue system

That's the advantages of cloud computing (AWS, Azure, CGP) and serverless (also auto-scaling application) applications.

#### 4. What is the difference between "limits" and "reservations"
- **Limits**: Avoid use beyond the proposed resources
- **Reserve** : Planned for a specific number of resources

#### 5.  Explain what those two parameters are used for.

- **Xms** : The amount of memory used by the heap when the machine starts,
- **Xmx** : parameter is used to specify the maximum amount of memory the heap can use when the machine is running.


Heap memory is the data area for all classes instances and arrays of the application. A lower Heap mean a higher garbage collection frequency
