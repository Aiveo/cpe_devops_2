# TD 1
## 1.

```json
GET _cluster/health
```

## 2. Create your first index
Create recipe index

```json
PUT /recipes
{
  "mappings": {
    "properties": {
      "name": { "type": "text" },
      "ingredients": { "type": "keyword" },
      "kitchen_tools": { "type": "keyword" },
      "preparation_time": { "type": "integer" },
      "cook_time": { "type": "integer" }
    }
  }
}
```

## 3. Index your first document
```json
POST /recipes/_doc
{
   "name":"Poulet au curry",
   "ingredients":[
      "blanc de poulet",
      "curry",
      "oignons",
      "poivre",
      "crème fraîche",
      "sel"
   ],
   "kitchen_tools":[
      "poêle",
      "couteau"
   ],
   "preparation_time":5,
   "cook_time":15
}
```

```json
GET /recipes/_search
```

## 4. Index a collection of documents
```json
POST /recipes/_bulk
{ "index": {} }
{"name": "Pâtes à la sauce tomate", "ingredients": ["pâtes","sauce tomate","sel"], "kitchen_tools": ["pan" ], "preparation_time": 1,"cook_time": 10}
{ "index": {} }
{ "name":"Boeuf bourguignon", "ingredients":[ "boeuf", "carottes", "farine", "bouillon", "oignons", "champignons", "vin rouge", "sel", "poivre" ], "kitchen_tools":[ "pressure cooker", "knife" ], "preparation_time":25, "cook_time":180 }
```

## 5. Search

```json
GET /recipes/_search
{
   "query":{
      "match":{
         "ingredients":"poivre"
      }
   }
}
```

```json
GET /recipes/_search
{
   "query":{
      "match":{
         "name":"boeuf"
      }
   }
}
```

```json
GET /recipes/_search
{
   "query":{
      "range":{
         "preparation_time":{
            "gte":1,
            "lte":15
         }
      }
   }
}
```

```json
GET /recipes/_search
{
   "query":{
      "bool":{
         "must":[
            {
               "range":{
                  "preparation_time":{
                     "gte":1,
                     "lte":15
                  }
               }
            },
            {
               "range":{
                  "cook_time":{
                     "gte":5,
                     "lte":10
                  }
               }
            }
         ]
      }
   }
}
```